import * as Animatable from 'react-native-animatable';
import {Image,TouchableWithoutFeedback,TouchableHighlight,Alert,Animated} from 'react-native';
import {Header,Left,Right} from 'native-base';
import { createDrawerNavigator,navigation,navigate } from 'react-navigation';
import React,{Component} from 'react';
import MyCart from './MyCart';

import Icon from 'react-native-vector-icons/FontAwesome';

class Animate extends Component {
  render() {
    return (
      <Right>
      <Animatable.View
      animation="slideInDown" iterationCount={1} direction="alternate" style={{flexDirection:'row'}}
      >
        {/* <Image source={require('./img/bell.jpg')} style={{ width: 30, height: 30 }} /> */}
      <Icon  name="bell-o" style={{ width: 30, height: 30 }}  />
        {/* <Image source={require('./img/cart.png')} style={{ width: 30, height: 30 }} /> */}
      <Icon name="shopping-cart" style={{ width: 30, height: 30 }} />
        </Animatable.View>
        </Right>
    );
  }
}

export default Animate;