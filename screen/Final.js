import React, {Component} from 'react';
import {Platform, StyleSheet, Image,Text, ScrollView,ImageBackground,TouchableOpacity,Animated,View} from 'react-native';
import {createDrawerNavigator} from 'react-navigation';
import {Header,Left,Right,Icon} from 'native-base';
import Animate from './Animat';
import CatSport from './Categ/CatSport';

class Final extends Component{
  static navigationOptions =  {
    drawerIcon: ({tintColor}) => (
      <View style={{
        marginLeft:5,
        height: 45,
        width: 45,
        backgroundColor:'#f43612',
        borderRadius: 25,
        alignItems: 'center',
        justifyContent: 'center'
      }}>
      <Icon name="heart" style={{fontSize:20, color:'white'}} />
      </View>
    )

  }
  render() {
    return (
      <View style={styles.container}>
        <Header style={{backgroundColor:"white"}}>
          <Left>
            <Icon name="menu" onPress={()=> this.props.navigation.openDrawer()} />
          </Left>
          <Animate />
        </Header>
        <ScrollView>
        <View style={{zIndex:-5,shadowOpacity:100,shadowColor:'black'}}>
      <ImageBackground style={{ width: 420, height:100,resizeMode:'strech',marginBottom:20}} source={require('./img/7w.jpg')} >
<Text style={{fontSize:20,color:'white',fontWeight:'bold',marginLeft:10,marginTop:20,marginBottom:10}}> {this.props.name}</Text>
</ImageBackground>      
        </View>
        <View style={{backgroundColor:'#f5f5f5'}}>
        <ScrollView
          horizontal={true}
          showsHorizontalScrollIndicator={false}
          decelerationRate={0}
          pagingEnabled
          snapToAlignment={"center"}          
        >
          <CatSport imageUri={require('../screen/img/6t.jpg')}
          
          />

          <CatSport imageUri={require('../screen/img/event2.jpg')}
         
          />

          <CatSport imageUri={require('../screen/img/event3.jpg')}
        
          />

          <CatSport imageUri={require('../screen/img/event2.jpg')}
         
          />
        </ScrollView>
        <View>
        <Image style={{height:50,width:400}} source={require('./img/line.png')} />
        </View>
        <View style={{elevation:10, borderColor:'grey', backgroundColor:'#f5f5f5', paddingBottom:50, paddingTop:20,paddingLeft:10, margin: 22}}>
<View style={{flexDirection:'row',marginTop:10,marginLeft:20}}>
            <View>
                    {/* twoText */}
                    <Text>02 Feb</Text>
                    <Text>2023</Text>
            </View>
<View>
    {/* LIne */}
<Image style={{width:30,height:30}} source={require('./img/Capture.jpg')} />
</View>
<View>
<Text>Cameroun - Gabon</Text>
<Text> 20:00 - Stade Omnisport</Text>
    {/* twoText large*/}
</View>
<View>
<Image style={{width:30,height:30}} source={require('./img/Capture.jpg')} />
    {/* LIne */}
</View>
<View>
    <Text style={{color:'red'}}>500</Text>
</View>
</View>
<View>
<Image style={{height:50,width:300}} source={require('./img/line.png')} />
 </View>


 <View style={{flexDirection:'row',marginTop:10,marginLeft:20}}>
            <View>
                    {/* twoText */}
                    <Text>02 Feb</Text>
                    <Text>2023</Text>
            </View>
<View>
    {/* LIne */}
<Image style={{width:30,height:30}} source={require('./img/Capture.jpg')} />
</View>
<View>
<Text>Cameroun - Gabon</Text>
<Text> 20:00 - Stade Omnisport</Text>
    {/* twoText large*/}
</View>
<View>
<Image style={{width:30,height:30}} source={require('./img/Capture.jpg')} />
    {/* LIne */}
</View>
<View>
    <Text style={{color:'red'}}>500</Text>
</View>
</View>
<View>
<Image style={{height:50,width:300}} source={require('./img/line.png')} />
 </View>


 <View style={{flexDirection:'row',marginTop:10,marginLeft:20}}>
            <View>
                    {/* twoText */}
                    <Text>02 Feb</Text>
                    <Text>2023</Text>
            </View>
<View>
    {/* LIne */}
<Image style={{width:30,height:30}} source={require('./img/Capture.jpg')} />
</View>
<View>
<Text>Cameroun - Gabon</Text>
<Text> 20:00 - Stade Omnisport</Text>
    {/* twoText large*/}
</View>
<View>
<Image style={{width:30,height:30}} source={require('./img/Capture.jpg')} />
    {/* LIne */}
</View>
<View>
    <Text style={{color:'red'}}>500</Text>
</View>
</View>
<View>
<Image style={{height:50,width:300}} source={require('./img/line.png')} />
 </View>



 <View style={{flexDirection:'row',marginTop:10,marginLeft:20}}>
            <View>
                    {/* twoText */}
                    <Text>02 Feb</Text>
                    <Text>2023</Text>
            </View>
<View>
    {/* LIne */}
<Image style={{width:30,height:30}} source={require('./img/Capture.jpg')} />
</View>
<View>
<Text>Cameroun - Gabon</Text>
<Text> 20:00 - Stade Omnisport</Text>
    {/* twoText large*/}
</View>
<View>
<Image style={{width:30,height:30}} source={require('./img/Capture.jpg')} />
    {/* LIne */}
</View>
<View>
    <Text style={{color:'red'}}>500</Text>
</View>
</View>
<View>
<Image style={{height:50,width:300}} source={require('./img/line.png')} />
 </View>

          </View>
        
        </View>
        <View>
        <Image style={{height:50,width:400}} source={require('./img/line.png')} />
        </View>
        </ScrollView>
      </View>
    );
}}

export default Final;

const styles = StyleSheet.create({
    container: {
    backgroundColor:'#f5f5f5' ,
      flex: 1
      
    }
  })