import React, { Component } from 'react';
import { Platform, ScrollView, StyleSheet, Text,Dimensions, Image, View, TouchableOpacity } from 'react-native';
import { createDrawerNavigator , createStackNavigator} from 'react-navigation';
import { Header, Left, Right, Icon } from 'native-base';
import * as Animatable from 'react-native-animatable';
import Animate from './Animat';
import Categories from './Categ/Categories';
import Cat2 from './Categ/Cat2';
import Cat3 from './Categ/Cat3';
import Cat4 from './Categ/Cat4';
import Sport from './Sport'
import Final from './Final'
import Film from './Film'
class Home extends Component {
  static navigationOptions = {
    drawerIcon: ({ tintColor }) => (
      <View style={{
        marginLeft:5,
        height: 45,
        width: 45,
        backgroundColor:'#55f77c',
        borderRadius: 25,
        alignItems: 'center',
        justifyContent: 'center'
      }}>
      <Icon name="home" style={{fontSize:20, color:'white'}} />
      </View>
    )

  }
  onSportPress(){

  }
  render() {
    let screenWidth = Dimensions.get('window').width;
    return (
      <ScrollView>
        <View style={styles.container}>
          <Header style={{ backgroundColor: "white" }}>
            <Left>
              <Icon name="menu" onPress={() => this.props.navigation.openDrawer()} />
            </Left>
          <Animate />
          </Header>
        </View>
        <ScrollView
          horizontal={true}
          showsHorizontalScrollIndicator={false}
          decelerationRate={0}
          pagingEnabled
          snapToAlignment={"center"}
        >
          <Categories imageUri={require('../screen/img/6t.jpg')}  
         name="25 Aug 2018 - Docuala"
          />

          <Categories imageUri={require('../screen/img/event2.jpg')}
          name="12 Aug 2018 - Docuala"
          />

          <Categories imageUri={require('../screen/img/event3.jpg')}
          name="10 Aug 2018 - Docuala"
          />

          <Categories imageUri={require('../screen/img/event2.jpg')}
          name="16 Aug 2018 - Docuala"
          />

        </ScrollView>

        <View style={{ marginTop: 10}}>
          <View>
            <Text style={{ color: 'black', marginLeft:10,fontFamily: '30',fontSize:20, fontWeight: '700', marginBottom: 10 ,fontStyle:'normal'}}>Catégories</Text>
          </View>
              <ScrollView
                horizontal={true}
                showsHorizontalScrollIndicator={false}
              >
                <Cat2 imageUri={require('../screen/img/bg-b.jpg')}
                  name="BUSINESS"
                />
                <Cat2 imageUri={require('../screen/img/bg-concrt.jpg')}
                  name="CONCERT"
                />

                <TouchableOpacity
                  onPress={()=>{this.props.navigation.navigate('Sport')}}
                >
                <Cat2 imageUri={require('../screen/img/bg-sport.jpg')}
                  name="SPORT"
                />
                </TouchableOpacity>

              <TouchableOpacity
                 onPress={()=>{this.props.navigation.navigate('Film')}}
              >
                <Cat2 imageUri={require('../screen/img/bg-c.jpg')}
                  name="CINEMA"
                />
              </TouchableOpacity>
                <Cat2 imageUri={require('../screen/img/bg-club.jpg')}
                  name="CLUBBING"
                />
                <Cat2 imageUri={require('../screen/img/bg-festival.jpg')}
                  name="FESTIVAL"
                />
                <Cat2 imageUri={require('../screen/img/bg-deals.jpg')}
                  name="DEALS"
                />
                <Cat2 imageUri={require('../screen/img/priv.jpg')}
                  name="PRIVATE"
                />
              </ScrollView>
           <HomeNavigator />
            <View>
              <Text style={{ color: 'black',fontSize:20, fontFamily: '30', fontWeight: '700', marginBottom: 10, marginTop:15, marginLeft:15,fontStyle:'normal' }}>Zoom sur</Text>
            </View>
            <View>
              <ScrollView
                horizontal={true}
                showsHorizontalScrollIndicator={false}
              >
                <Cat3 imageUri={require('../screen/img/klax1.png')}
                  name="DJ KLAXS"
                />

                <Cat3 imageUri={require('../screen/img/blanche1.png')}
                  name="BLANCHI BAILLY"
                />

                <Cat3 imageUri={require('../screen/img/ztra1.png')}
                  name="ZTRA"
                />


                <Cat3 imageUri={require('../screen/img/le1.png')}
                  name="LE CAID"
                />

                <Cat3 imageUri={require('../screen/img/nabila1.png')}
                  name="NABILA"
                />

              </ScrollView>

            </View>
          <View>
              <View>
                <Text style={{color: 'black',fontSize:20, fontFamily: '30', fontWeight: '700', marginBottom: 10, marginTop:15, marginLeft:15,fontStyle:'normal'}} >Promoteurs</Text>
              </View>
            <View>
            <ScrollView
                horizontal={true}
                showsHorizontalScrollIndicator={false}
              >
                <Cat4 imageUri={require('../screen/img/bg-promoter1.jpg')}
                  name="Jecam"
                  namet="Douala"
                />

                <Cat4 imageUri={require('../screen/img/bg-promoter2.jpg')}
                  name="Ralia"
                  namet="Douala"
                />

                <Cat4  imageUri={require('../screen/img/bg-promoter3.jpg')}
                  name="Satunnas"
                  namet="Douala"
                />


                <Cat4  imageUri={require('../screen/img/bg-promoter4.jpg')}
                  name="Kmer Hub"
                  namet="Douala"
                />

                <Cat4 imageUri={require('../screen/img/bg-promoter5.jpg')}
                  name="Opium"
                  namet="Douala"
                />

              </ScrollView>


            </View>
          </View>
            </View>
      </ScrollView>
    );
  }
}


const HomeNavigator = createStackNavigator({
    Sport:Sport,
    Film,Film
    //Final:Final
})

export default Home;


const styles = StyleSheet.create({
  container: {
    flex: 1
  }
})