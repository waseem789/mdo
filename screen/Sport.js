import React, {Component} from 'react';
import {Platform, StyleSheet, Image,Text, ScrollView,ImageBackground,TouchableOpacity,Animated,View} from 'react-native';
import {createDrawerNavigator,createStackNavigator} from 'react-navigation';
import {Header,Left,Right,Icon} from 'native-base';
import Animate from './Animat';
import CatSport from './Categ/CatSport';
import Final from './Final';
import Btn  from './Categ/Btn';
import Film from './Film'

class Sport extends Component{
  render() {
    return (
      <View style={styles.container}>
        <Header style={{backgroundColor:"white"}}>
          <Left>
            <Icon name="menu" onPress={()=> this.props.navigation.openDrawer()} />
          </Left>
          <Animate />
        </Header>
        <ScrollView>
        <View style={{zIndex:-5,shadowOpacity:100,shadowColor:'black'}}>
      <ImageBackground style={{ width: 420, height:100,resizeMode:'strech',marginBottom:20}} source={require('./img/7w.jpg')} >
<Text style={{fontSize:20,color:'white',fontWeight:'bold',marginLeft:10,marginTop:20,marginBottom:10}}>Sports</Text>
</ImageBackground>      
        </View>
        <View>
        <ScrollView
          horizontal={true}
          showsHorizontalScrollIndicator={false}
          decelerationRate={0}
          pagingEnabled
          snapToAlignment={"center"}          
        >
          <CatSport imageUri={require('../screen/img/6t.jpg')}
          
          />

          <CatSport imageUri={require('../screen/img/event2.jpg')}
         
          />

          <CatSport imageUri={require('../screen/img/event3.jpg')}
        
          />

          <CatSport imageUri={require('../screen/img/event2.jpg')}
         
          />
        </ScrollView>
        <View>
        <Image style={{height:100,width:400}} source={require('./img/line.png')} />
        </View>
        <View style={{elevation:10, borderColor:'grey', backgroundColor:'#f5f5f5', paddingBottom:50, paddingTop:20,paddingLeft:10, margin: 22}}>
      
   <TouchableOpacity
            style={styles.button}
            onPress={()=>{this.props.navigation.navigate('Final')}}
            activeOpacity={1}>
          <Text style={styles.text}>  8/FINALE </Text>
          </TouchableOpacity>
         
              <TouchableOpacity
              style={styles.button1}
              onPress={()=>{this.props.navigation.navigate('Final')}}
              activeOpacity={1}>

                <Text style={styles.text}>1/4 FINALE</Text>
              </TouchableOpacity>


              <TouchableOpacity
              style={styles.button2}
              onPress={()=>{this.props.navigation.navigate('Final')}}
              activeOpacity={1}>

                <Text style={styles.text}>DEMI FINALE</Text>
              </TouchableOpacity>

              <TouchableOpacity
              style={styles.button3}
              onPress={()=>{this.props.navigation.navigate('Final')}}
              activeOpacity={1}>

                <Text style={styles.text}>3EME PLACE</Text>
              </TouchableOpacity>

              <TouchableOpacity
              style={styles.button4}
              onPress={()=>{this.props.navigation.navigate('Final')}}
              activeOpacity={1}>
              <Text style={styles.text}>FINAL</Text>
              </TouchableOpacity>
          </View>
          
        </View>

        <View>
        <Image style={{height:100,width:400}} source={require('./img/line.png')} />
        </View>
        </ScrollView>
      </View>
    );
  }
}

const HomeNavigator = createStackNavigator({
  Final:Final,
  Film,Film
},{

Header:false

})

export default Sport;


const styles = StyleSheet.create({
    container: {
      flex: 1
    }, 
    button: {
      alignItems: 'center',
      marginTop:15,
      justifyContent: 'center',
      backgroundColor: '#4b95e9',
      height:40,
      width:200,
      marginLeft:70,
      borderRadius: 20,
    },
    button1: {
      alignItems: 'center',
      marginTop:15,
      justifyContent: 'center',
      backgroundColor: '#49b578',
      height:40,
      width:200,
      marginLeft:70,
      borderRadius: 20,
    },
    button2: {
      alignItems: 'center',
      marginTop:15,
      justifyContent: 'center',
      backgroundColor: '#b04eb9',
      height:40,
      width:200,
      marginLeft:70,
      borderRadius: 20,
    },
    button3: {
      alignItems: 'center',
      marginTop:15,
      justifyContent: 'center',
      backgroundColor: '#464e69',
      height:40,
      width:200,
      marginLeft:70,
      borderRadius: 20,
    },
    button4: {
      alignItems: 'center',
      marginTop:15,
      justifyContent: 'center',
      backgroundColor: '#7c8c7c',
      height:40,
      width:200,
      marginLeft:70,
      borderRadius: 20,
    },
    text: {
      color: 'white',
      backgroundColor: 'transparent',
    }
  })