import React, {Component} from 'react';
import {Platform, StyleSheet,Image, Text, View} from 'react-native';
import {createDrawerNavigator} from 'react-navigation';
import {Header,Left,Right,Icon} from 'native-base';
import * as Animatable from 'react-native-animatable';
import Animate from './Animat';
class Terms extends Component{
  static navigationOptions =  {
    drawerIcon: ({tintColor}) => (
      <View style={{
        marginLeft:5,
        height: 45,
        width: 45,
        backgroundColor:'#bf1bd3',
        borderRadius: 25,
        alignItems: 'center',
        justifyContent: 'center'
      }}>
      <Icon name="bulb" style={{fontSize:20, color:'white'}} />
      </View>
    )

  }
  render() {
    return (
      <View style={styles.container}>
        <Header style={{backgroundColor:"white"}}>
          <Left>
            <Icon name="menu" onPress={()=> this.props.navigation.openDrawer()} />
          </Left>
          <Animate />
        </Header>
<Text>Terms Here</Text>
      </View>
    );
  }
}

export default Terms ;


const styles = StyleSheet.create({
    container: {
      flex: 1
    }
  })