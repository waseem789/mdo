import React, {Component} from 'react';
import {Platform, StyleSheet, Image,Text, ImageBackground,View} from 'react-native';
import {createDrawerNavigator} from 'react-navigation';
import {Header,Left,Right,Icon} from 'native-base';
import * as Animatable from 'react-native-animatable';
import Animate from './Animat';
class Wishlist extends Component{
  static navigationOptions =  {
    drawerIcon: ({tintColor}) => (
      <View style={{
        marginLeft:5,
        height: 45,
        width: 45,
        backgroundColor:'#f43612',
        borderRadius: 25,
        alignItems: 'center',
        justifyContent: 'center'
      }}>
      <Icon name="heart" style={{fontSize:20, color:'white'}} />
      </View>
    )

  }
  render() {
    return (
      <View style={styles.container}>
        <Header style={{backgroundColor:"white"}}>
          <Left>
            <Icon name="menu" onPress={()=> this.props.navigation.openDrawer()} />
          </Left>
          <Animate />
        </Header>
        <View style={{zIndex:-5,shadowOpacity:100,shadowColor:'black'}}>
      <ImageBackground style={{ width: 420, height:100,resizeMode:'strech'}} source={require('./img/7w.jpg')} >
<View  style={{flexDirection:'row'}}>
<View>
<Text style={{fontSize:20,color:'white',fontWeight:'bold',marginLeft:10,marginTop:20,marginBottom:10}}>Wishlist</Text>
<Text style={{color:'white',marginLeft:10}}>The events you wish to attend to </Text>
</View>
<View style={{marginTop:25,marginLeft:100}}>
<Icon name="heart" style={{color:'white'}} onPress={() => this.props.navigation.openDrawer()} />
</View>
</View>
</ImageBackground>      
        </View>
      </View>
    );
  }
}

export default Wishlist;


const styles = StyleSheet.create({
    container: {
      flex: 1
    }
  })