import React, {Component} from 'react';
import {Platform,TouchableOpacity,ScrollView, StyleSheet, Text,Image, View} from 'react-native';
import {createDrawerNavigator} from 'react-navigation';
import {Header,Left,Right,Icon} from 'native-base';
import * as Animatable from 'react-native-animatable';
import Animate from './Animat';
import spinner from './img/spin.gif';
class Profile extends Component{
  static navigationOptions =  {
    drawerIcon: ({tintColor}) => (
      <View style={{
        marginLeft:5,
        height: 45,
        width: 45,
        backgroundColor:'#d3d3d3',
        borderRadius: 25,
        alignItems: 'center',
        justifyContent: 'center'
      }}>
      <Icon name="person" style={{fontSize:20, color:'white'}} />
      </View>
    )
  }
  render() {
    return (
      <View style={styles.container}>
        <Header style={{backgroundColor:"white"}}>
          <Left>
            <Icon  size={20} name="menu" onPress={()=> this.props.navigation.openDrawer()} />
          </Left>
          <Animate />
        </Header>
        <ScrollView>
        <View style={{width:350,height:330,marginBottom:20,alignItems:'center'}}>
        <Image style={{width:350,height:330 ,marginLeft:10, marginTop:15,marginBottom:20,alignItems:'center'}} source={require('./img/profile.png')} />
        </View>

        <View  style ={styles.shadow} >
     <View style={{marginLeft:10,marginTop:20,fontSize:20}}>
      <Text>Email:</Text>
      <Text>Phone:</Text>
      <Text>BirthDate:</Text>
      <Text>Location</Text>
      <Text>Gender</Text>
      </View>
      <View style={{marginLeft:50,marginTop:20,fontSize:20}}>
      <Text>mework340@gmail.com</Text>
      <Text>0349-1466132</Text>
      <Text>9-September-1995</Text>
      <Text>Johar Toen Lahore</Text>
      <Text>Male</Text>
      </View>
        </View>
        <View style={{width:150,height:100,marginLeft:110,marginTop:15}}>
        <TouchableOpacity
            style={styles.button}
            onPress={this._onPress}
            activeOpacity={1}>
        
              <Text style={styles.text}>Edit</Text>
          </TouchableOpacity>
        </View>
        <View style={{width:150,height:100,marginLeft:110}}>
        <TouchableOpacity
            style={styles.button1}
            onPress={this._onPress}
            activeOpacity={1}>
           
              <Text style={styles.text}>Delete Account</Text>
          </TouchableOpacity>
        </View>
        </ScrollView>
      </View>
    );
  }
}

export default Profile;


const styles = StyleSheet.create({
    container: {
      flex: 1
    },
    shadow:{
      width:300,
      height:150,
    borderWidth: 1,
    borderRadius: 2,
    borderColor: '#ddd',
    borderBottomWidth: 0,
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.8,
    shadowRadius: 2,
    elevation: 1,
    marginLeft:30,
    flexDirection:'row',
    marginTop:20,
    marginRight: 5,
    marginTop: 10,
    },
    button: {
      alignItems: 'center',
      justifyContent: 'center',
      backgroundColor: '#00bfff',
      height:50,
      borderRadius: 20,
      zIndex: 100,
      shadowColor: '#00bfff',
    shadowOffset: { width:10, height:10},
    shadowOpacity: 5,
    shadowRadius: 10,
    },
    button1: {
       marginTop:0,
      alignItems: 'center',
      justifyContent: 'center',
      backgroundColor: 'red',
      height:50,
      borderRadius: 20,
      zIndex: 100,
      shadowColor: 'red',
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.8,
    shadowRadius: 2,
    },
    image: {
      width: 24,
      height: 24,
    },
    text: {
      color: 'white',
      backgroundColor: 'transparent',
    }
  })