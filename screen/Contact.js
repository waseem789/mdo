import React, {Component} from 'react';
import {Platform, StyleSheet,TextInput, TouchableOpacity,Image,Text, View} from 'react-native';
import {createDrawerNavigator} from 'react-navigation';
import {Header,Left,Right,Icon} from 'native-base';
import * as Animatable from 'react-native-animatable';
import Animate from './Animat';
class Contact extends Component{
  static navigationOptions =  {
    drawerIcon: ({tintColor}) => (
      <View style={{
        marginLeft:5,
        height: 45,
        width: 45,
        backgroundColor:'#54a5e5',
        borderRadius: 25,
        alignItems: 'center',
        justifyContent: 'center'
      }}>
      <Icon name="bookmarks" style={{fontSize:20, color:'white'}} />
      </View>
    )

  }
  render() {
    return (
      <View style={styles.container}>
        <Header style={{backgroundColor:"white"}}>
          <Left>
            <Icon name="menu" onPress={()=> this.props.navigation.openDrawer()} />
          </Left>
          <Animate />
        </Header>
        <View style={{elevation:10, borderColor:'grey', backgroundColor:'white', paddingBottom:50, paddingTop:20, marginTop:20,paddingLeft:10, margin: 22}}>
        <View style={{marginLeft:100,color:'black',fontSize:20}}>
          <Text style={{color:'black',marginBottom:15,fontWeight:'bold',fontSize:20}}> Get in touch !</Text>
        </View>
        <View>

          <Image style={{width:200,height:100}} source={require('./img/mm.jpg')}/>
        </View>
        <View style={{marginTop:10}}>
        <Text>Name:</Text>
        </View>
        
        <View style={{marginTop:10}}>
        <TextInput style={{width:300,height:30,borderColor:'grey',borderWidth:1}}/>
        </View>

        <View style={{marginTop:10}}>
        <Text>Email:</Text>
        </View>
        
        <View style={{marginTop:10}}>
        <TextInput style={{width:300,height:30,borderColor:'grey',borderWidth:1}}/>
        </View>

        <View style={{marginTop:10}}>
        <Text>Message:</Text>
        </View>
        
        <View style={{marginTop:10}}>
        <TextInput style={{width:300,height:60,borderColor:'grey',borderWidth:1}}/>
        </View>

        <TouchableOpacity
            style={styles.button}
            onPress={this._onPress}
            activeOpacity={1}>
        
              <Text style={styles.text}>Send Message </Text>
          </TouchableOpacity>

        </View>
      </View>
    );
  }
}

export default Contact;


const styles = StyleSheet.create({
    container: {
      flex: 1
    },
    button: {
      alignItems: 'center',
      justifyContent: 'center',
      backgroundColor: '#96CB7F',
      height:40,
      width:300,
      marginRight:5,
      marginTop:15,
      zIndex: 100,
      shadowColor: '#00bfff',
    shadowOffset: { width:10, height:10},
    shadowOpacity: 5,
    shadowRadius: 10,
    }
  })