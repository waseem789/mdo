import React, {Component} from 'react';
import {Platform, StyleSheet, Image,Text,ScrollView,TouchableOpacity, ImageBackground,View} from 'react-native';
import {createDrawerNavigator,createStackNavigator,createSwitchNavigator} from 'react-navigation';
import {Header,Left,Right} from 'native-base';
import * as Animatable from 'react-native-animatable';
import MyFilm from './Categ/FilmCat'
import Animate from './Animat';
import SelectFilm from './SelectFilm'
import Icon from 'react-native-vector-icons/Entypo';
import Final from './Final';
class Film extends Component{
  static navigationOptions =  {
    drawerIcon: ({tintColor}) => (
      <View style={{
        marginLeft:5,
        height: 45,
        width: 45,
        backgroundColor:'#f43612',
        borderRadius: 25,
        alignItems: 'center',
        justifyContent: 'center'
      }}>
      <Icon name="heart" style={{fontSize:20, color:'white'}} />
      </View>
    )

  }
  render() {
    return (
      <View style={styles.container}>
      < AppNavigator />
        <Header style={{backgroundColor:"white"}}>
          <Left>
            <Icon name="menu" onPress={()=> this.props.navigation.openDrawer()} />
          </Left>
          <Animate />
        </Header>
        <View style={{zIndex:-5,shadowOpacity:100,shadowColor:'black'}}>
      <ImageBackground style={{ width: 420, height:100,resizeMode:'strech'}} source={require('./img/7w.jpg')} >
<View  style={{flexDirection:'row'}}>
<View>
<Text style={{fontSize:20,color:'white',fontWeight:'bold',marginLeft:10,marginTop:20,marginBottom:10}}>CANAL OLYMPIA YAOUNDE</Text>
</View>
</View>
</ImageBackground>      
        </View>
        <View>
        <Image style={{height:30,width:400}} source={require('./img/line.png')} />
        </View>
        <View style={{elevation:10, borderColor:'grey', backgroundColor:'#f5f5f5', paddingBottom:50, paddingTop:20,paddingLeft:10, margin: 22}}>
        <View>
        <Text>Canal Olympia - Institut Francais - Douala bercy</Text>
        <Text style={{color:'black',fontWeight:'bold',fontSize:24,marginTop:20}}>Films et événements</Text>
        <View style={{flexDirection:'row'}}>
        <Text> A l' affiche</Text>
        <Icon style={{color:'black',fontSize:20,marginLeft:10}} name="chevron-down" />
        </View>
      
        </View>

        <ScrollView
         horizontal={true}
         showsHorizontalScrollIndicator={false}
         decelerationRate={0}
         pagingEnabled
        
        >
        <View style={{flexDirection:'row',marginTop:20}}>
        <TouchableOpacity
       onPress={()=>{this.props.navigation.navigate('SelectFilm')}}
        >
        <MyFilm imageUri={require('../screen/img/film.jpg')}  
        name="Avenger"
        />
        </TouchableOpacity>
        <TouchableOpacity
        onPress={()=>{this.props.navigation.navigate('SelectFilm')}}
        >
        <MyFilm imageUri={require('../screen/img/film1.jpg')}  
        name="Thor"
        />
        </TouchableOpacity>
        <TouchableOpacity
        onPress={()=>{this.props.navigation.navigate('SelectFilm')}}
        >
        <MyFilm imageUri={require('../screen/img/film2.jpg')}  
        name="Iron Man"
        />
        </TouchableOpacity>
        <TouchableOpacity
        onPress={()=>{this.props.navigation.navigate('SelectFilm')}}
        >
        <MyFilm imageUri={require('../screen/img/film3.jpg')} 
        name="Aqua Man"
        />
        </TouchableOpacity>
        </View>
    
        </ScrollView>
        <Text style={{color:'black',fontWeight:'bold',fontSize:24,marginTop:50}}> Evénements</Text>
        </View>
      </View>
    );
  }
}

export default Film;
const AppNavigator = createStackNavigator({
  Final:Final,
  Film:Film,
  SelectFilm:SelectFilm

})


const styles = StyleSheet.create({
    container: {
      flex: 1
    }
  })