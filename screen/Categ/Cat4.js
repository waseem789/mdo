import React, {Component} from 'react';
import { Header, Left, Right, Icon } from 'native-base';
import {Platform,Image,ScrollView,StyleSheet, Text, View} from 'react-native';
import {createBottomTabNavigator } from 'react-navigation';

class Cat4 extends Component {
  render() {
    return (
        <View  style={{height:180,width:130,marginRight:10}}>
        <View style={{flex:2}}>
        <Image style={{ width: null, height:null,flex:1,resizeMode:'cover' }} source={this.props.imageUri} />
        </View>
        <View style={{flex:1,paddingLeft:10}}>
        <Text style={{color:'black',marginLeft:20,marginBottom:2}}>{this.props.name}</Text>
        <View style={{backgroundColor:'#D3D3D3',height:20,flexDirection:'row',width:100,borderRadius:5}}> 
        <Text style={{marginLeft:30}}>2.5</Text>
        <Icon name="star" style={{ marginLeft:5,fontSize:17}} />
        </View> 
        <Text style={{color:'black',marginLeft:20,marginTop:2}}>{this.props.namet}</Text>
        </View>
        </View>
    );
  }
}
export default Cat4;
