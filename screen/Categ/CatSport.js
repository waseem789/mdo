import React, {Component} from 'react';
import {Platform,Image,ImageBackground,Dimensions,ScrollView,StyleSheet, Text, View} from 'react-native';
import {createBottomTabNavigator } from 'react-navigation';

class CatSport extends Component {
  render() {
    let screenWidth = Dimensions.get('window').width;
    return (
        
        <View style={{flex:1,height:15}}>
        <ImageBackground style={{ width:screenWidth,height:150,flex:2,resizeMode:'cover' }} source={this.props.imageUri} >
        </ImageBackground>
        </View>
    );
  }
}
export default CatSport;
