import React, {Component} from 'react';
import {Platform,Image,ScrollView,StyleSheet, Text, View} from 'react-native';
import {createBottomTabNavigator } from 'react-navigation';

class FilmCat extends Component {
  render() {
    return (
        <View >
        <View style={{height:150,width:80,marginRight:5,marginLeft:10}} >
        <View style={{flex:4}}>
        <Image style={{ width:80, height:100,flex:1,resizeMode:'cover' }} source={this.props.imageUri} />
        </View>
        <View style={{flex:1}}>
        <Text style={{fontStyle:'normal',margin:10}}>{this.props.name}</Text>
        </View>
        </View>
        </View>
    );
  }
}
export default FilmCat;
