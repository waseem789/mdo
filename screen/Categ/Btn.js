import React, {Component} from 'react';
import {Platform,Image,ScrollView,StyleSheet, Text, View,TouchableOpacity} from 'react-native';
import { createStackNavigator } from 'react-navigation';
import Final from '../Final'
import { Button } from 'native-base';

class Btn extends Component {
  render() {

  }
}


export default Btn;
const styles = StyleSheet.create({
    container: {
      flex: 1
    }, 
    button: {
      alignItems: 'center',
      marginTop:15,
      justifyContent: 'center',
      backgroundColor: '#4b95e9',
      height:40,
      width:200,
      marginLeft:70,
      borderRadius: 20,
    },
    button1: {
      alignItems: 'center',
      marginTop:15,
      justifyContent: 'center',
      backgroundColor: '#49b578',
      height:40,
      width:200,
      marginLeft:70,
      borderRadius: 20,
    },
    button2: {
      alignItems: 'center',
      marginTop:15,
      justifyContent: 'center',
      backgroundColor: '#b04eb9',
      height:40,
      width:200,
      marginLeft:70,
      borderRadius: 20,
    },
    button3: {
      alignItems: 'center',
      marginTop:15,
      justifyContent: 'center',
      backgroundColor: '#464e69',
      height:40,
      width:200,
      marginLeft:70,
      borderRadius: 20,
    },
    button4: {
      alignItems: 'center',
      marginTop:15,
      justifyContent: 'center',
      backgroundColor: '#7c8c7c',
      height:40,
      width:200,
      marginLeft:70,
      borderRadius: 20,
    },
    text: {
      color: 'white',
      backgroundColor: 'transparent',
    }
  })