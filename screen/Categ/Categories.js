import React, {Component} from 'react';
import {Platform,Image,ImageBackground,Dimensions,ScrollView,StyleSheet, Text, View} from 'react-native';
import {createBottomTabNavigator } from 'react-navigation';

class Categories extends Component {
  render() {
    let screenWidth = Dimensions.get('window').width;
    return (
        <View
        style={{borderWidth:0.5}}
        >
        <View style={{flex:2}}>
        <ImageBackground style={{ width:screenWidth,height:300,flex:1,resizeMode:'cover' }} source={this.props.imageUri} >
        <View >
        <Text style={{color:'white',fontWeight:'bold',marginTop:250,marginLeft:20,fontStyle:'normal'}}>{this.props.name}</Text>
        </View>
        </ImageBackground>
        </View>
     </View>
    );
  }
}
export default Categories;
