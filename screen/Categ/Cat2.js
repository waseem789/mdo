import React, {Component} from 'react';
import {Platform,Image,ImageBackground,ScrollView,StyleSheet, Text, View} from 'react-native';
import {createBottomTabNavigator } from 'react-navigation';

class Cat2 extends Component {
  render() {
    return (
      <View >
        <View style={{height:150,width:150,marginRight:10}}>
        <View style={{flex:2}}>
        <ImageBackground style={{ width: null, height:null,flex:1,resizeMode:'cover' }} source={this.props.imageUri}
        >
        <Text style={{color:'black',marginTop:100,paddingLeft:50,flex:1,fontWeight:'bold',fontStyle:'normal'}}>{this.props.name} </Text>
        </ImageBackground>
        </View>
        </View>
        </View>
    );
  }
}
export default Cat2;
