import React, {Component} from 'react';
import {Platform,Image,ScrollView,StyleSheet, Text, View} from 'react-native';
import {createBottomTabNavigator } from 'react-navigation';

class Cat3 extends Component {
  render() {
    return (
        <View >
        <View style={{height:130,width:130,marginRight:10}} >
        <View style={{flex:3}}>
        <Image style={{ width: null, height:null,flex:1,resizeMode:'cover' }} source={this.props.imageUri} />
        </View>
        <View style={{flex:1}}>
        <Text style={{color:'black',textAlign:'center',fontStyle:'normal'}}>{this.props.name}</Text>
        </View>
        </View>
        </View>
    );
  }
}
export default Cat3;
