import React, {Component} from 'react';
import {Platform, StyleSheet,ScrollView,ImageBackground, Image,Text, View} from 'react-native';
import {createDrawerNavigator} from 'react-navigation';
import {Header,Left,Right} from 'native-base';
import * as Animatable from 'react-native-animatable';
import Icon from 'react-native-vector-icons/Entypo';
import Animate from './Animat';
class MyOrder extends Component{
  static navigationOptions =  {
    drawerIcon: ({tintColor}) => (
      <View style={{
        marginLeft:5,
        height: 45,
        width: 45,
        backgroundColor:'#e94c3d',
        borderRadius: 25,
        alignItems: 'center',
        justifyContent: 'center'
      }}>
      <Icon name="paper-plane" style={{fontSize:20, color:'white'}} />
      </View>
    )

  }
  render() {
    return (
      <View style={styles.container}>
        <Header style={{backgroundColor:"white"}}>
          <Left>
            <Icon size={20} name="menu" onPress={()=> this.props.navigation.openDrawer()} />
          </Left>
          <Animate/>
        </Header>
        <View style={{zIndex:-5,shadowOpacity:100,shadowColor:'black'}}>
      <ImageBackground style={{ width: 420, height:100,resizeMode:'strech'}} source={require('./img/7w.jpg')} >
<View  style={{flexDirection:'row'}}>
<View>
<Text style={{fontSize:20,color:'white',fontWeight:'bold',marginLeft:10,marginTop:20,marginBottom:10}}>My Tickets</Text>
</View>
<View style={{marginTop:25,marginLeft:220}}>
<Icon name="list" style={{color:'white'}} onPress={() => this.props.navigation.openDrawer()} />
</View>
</View>
</ImageBackground>      
        </View>
        <View>
        <Image style={{height:50,width:400}} source={require('./img/line.png')} />
        </View>
<ScrollView>
        <View style={{elevation:10, borderColor:'grey', backgroundColor:'white', paddingBottom:50, paddingTop:20, marginTop:20,paddingLeft:10, margin: 22}}>
      <Image style={{marginLeft:100,width:100,height:100}} source={require('./img/tic.png')} />
      <View style={{marginLeft:100}}> 
      <View>
      <Image style={{height:50,width:50,marginLeft:80,marginTop:5}} source={require('./img/vv.jpg')}/>  
      </View>
        <Text style={{fontWeight:'bold',fontSize:15,color:'blue',marginTop:15,marginBottom:10}}>Blanche Baily</Text>
        <View style={{flexDirection:'row'}}>
        <View  style={{marginTop:15}}>
          <Icon style={{marginBottom:5 ,marginRight:5,color:'blue'}} size={12} name="location" />
        </View>
        <View>
       <Text style={{fontWeight:'bold',fontSize:12,marginTop:15,color:'blue'}}>New York City</Text>
       </View>
       </View>

        <View style={{flexDirection:'row',marginTop:15}}>
       <View>
          <Icon style={{marginBottom:5, marginRight:5,color:'blue'}}  size={12} name="time-slot" />
        </View>
        <View>
       <Text style={{fontWeight:'bold',fontSize:12,color:'blue'}}>15th May 2039 | 20:00 | RSVP </Text>
       </View>
       </View>
              <View>
              <Image style={{height:50,width:50,marginLeft:80}} source={require('./img/vv.jpg')}/>  
              </View>

              <View> 
        <Text style={{fontWeight:'bold',fontSize:15,color:'blue',marginTop:15,marginBottom:10}}>Blanche Baily</Text>
        <View style={{flexDirection:'row'}}>
        <View  style={{marginTop:15}}>
          <Icon style={{marginBottom:5 ,marginRight:5,color:'blue'}} size={12} name="location" />
        </View>
        <View>
       <Text style={{fontWeight:'bold',fontSize:12,marginTop:15,color:'blue'}}>New York City</Text>
       </View>
       </View>

        <View style={{flexDirection:'row',marginTop:15}}>
       <View>
          <Icon style={{marginBottom:5, marginRight:5,color:'blue'}}  size={12} name="time-slot" />
        </View>
        <View>
       <Text style={{fontWeight:'bold',fontSize:12,color:'blue'}}>15th May 2039 | 20:00 | RSVP </Text>
       </View>
       </View>
              <View>
              <Image style={{height:50,width:50,marginLeft:80}} source={require('./img/vv.jpg')}/>  
              </View>
              </View>

              <View> 
        <Text style={{fontWeight:'bold',fontSize:15,color:'blue',marginTop:15,marginBottom:10}}>Blanche Baily</Text>
        <View style={{flexDirection:'row'}}>
        <View  style={{marginTop:15}}>
          <Icon style={{marginBottom:5 ,marginRight:5,color:'blue'}} size={12} name="location" />
        </View>
        <View>
       <Text style={{fontWeight:'bold',fontSize:12,marginTop:15,color:'blue'}}>New York City</Text>
       </View>
       </View>

        <View style={{flexDirection:'row',marginTop:15}}>
       <View>
          <Icon style={{marginBottom:5, marginRight:5,color:'blue'}}  size={12} name="time-slot" />
        </View>
        <View>
       <Text style={{fontWeight:'bold',fontSize:12,color:'blue'}}>15th May 2039 | 20:00 | RSVP </Text>
       </View>
       </View>
              <View>
              <Image style={{height:50,width:50,marginLeft:80}} source={require('./img/vv.jpg')}/>  
              </View>
       </View>
       </View>
      </View>
</ScrollView>
</View>
    );
  }
}
export default MyOrder;


const styles = StyleSheet.create({
    container: {
      flex: 1
    }
  })