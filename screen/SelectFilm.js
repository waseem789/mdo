import React, {Component} from 'react';
import {Platform, StyleSheet, Image,Text,ScrollView, ImageBackground,Dimensions,View} from 'react-native';
import {createDrawerNavigator} from 'react-navigation';
import {Header,Left,Right} from 'native-base';
import * as Animatable from 'react-native-animatable';
import MyFilm from './Categ/FilmCat'
import Animate from './Animat';
import Icon from 'react-native-vector-icons/Entypo';
class SelectFilm extends Component{
  static navigationOptions =  {
    drawerIcon: ({tintColor}) => (
      <View style={{
        marginLeft:5,
        height: 45,
        width: 45,
        backgroundColor:'#f43612',
        borderRadius: 25,
        alignItems: 'center',
        justifyContent: 'center'
      }}>
      <Icon name="heart" style={{fontSize:20, color:'white'}} />
      </View>
    )

  }
  render() {
    let screenWidth = Dimensions.get('window').width;
    return (
      <View style={styles.container}>
        <Header style={{backgroundColor:"white"}}>
          <Left>
            <Icon name="menu" onPress={()=> this.props.navigation.openDrawer()} />
          </Left>
          <Animate />
        </Header>
        <View style={{zIndex:-5,shadowOpacity:100,shadowColor:'black'}}>
      <ImageBackground style={{ width: 420, height:100,resizeMode:'strech'}} source={require('./img/7w.jpg')} >
<View  style={{flexDirection:'row'}}>
<View>
<Text style={{fontSize:20,color:'white',fontWeight:'bold',marginLeft:10,marginTop:20,marginBottom:10}}>Avengers</Text>
</View>
</View>
</ImageBackground>      
        </View>
   
        <Image  style={{width:screenWidth,height:200,marginTop:10}} source={require('./img/film.jpg')} />
        <View style={{elevation:10, borderColor:'grey', backgroundColor:'#f5f5f5', paddingBottom:50, paddingTop:20,paddingLeft:10, margin: 22}}>
            <View style={{flexDirection:'row'}}>
                <Image source={require('./img/film.jpg') } style={{height:150,width:80}}/>
            <View style={{marginLeft:20,color:'black'}}>
                <Text style={{marginBottom:20}} >Sortie: 23 october 2016</Text>
                <Text style={{marginBottom:20}} >Drame (2h 13)</Text>
                <Text style={{marginBottom:20}} >de Pawel moniup </Text>
            </View>
        </View>

        </View>
        </View> 

    );
  }
}

export default SelectFilm;


const styles = StyleSheet.create({
    container: {
      flex: 1
    }
  })