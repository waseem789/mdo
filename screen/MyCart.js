import React, {Component} from 'react';
import {Platform, StyleSheet,ImageBackground,Image,ScrollView,TextInput,TouchableOpacity, Text, View} from 'react-native';
import {createDrawerNavigator} from 'react-navigation';
import {Header,Left,Right} from 'native-base';
import * as Animatable from 'react-native-animatable';
import Icon from 'react-native-vector-icons/Entypo';
import Animate from './Animat';
class MyCart extends Component{
  static navigationOptions =  {
    drawerIcon: ({tintColor}) => (
    
      <View style={{
        marginLeft:5,
        height: 45,
        width: 45,
        backgroundColor:'#e94c3d',
        borderRadius: 25,
        alignItems: 'center',
        justifyContent: 'center'
      }}>
      <Icon name="shopping-cart" style={{fontSize:20, color:'white'}} />
      </View>
    )

  }
  render() {
    
    return (
      <View style={styles.container}>
        <Header style={{backgroundColor:"white"}}>
          <Left>
            <Icon size={20} name="menu" onPress={()=> this.props.navigation.openDrawer()} />
          </Left>
          <Animate />
        </Header>
<View style={{zIndex:-5,shadowOpacity:100,shadowColor:'black'}}>
      <ImageBackground style={{ width: 420, height:100,resizeMode:'strech'}} source={require('./img/7w.jpg')} >
<View  style={{flexDirection:'row'}}>
<View>
<Text style={{fontSize:20,color:'white',fontWeight:'bold',marginLeft:10,marginTop:20,marginBottom:10}}>My Deals Cart</Text>
<Text style={{color:'white',marginLeft:10}}>Check your current deals</Text>
</View>
<View style={{marginTop:25,marginLeft:190}}>
<Icon name="shopping-cart" style={{color:'white'}} onPress={() => this.props.navigation.openDrawer()} />
</View>
</View>
</ImageBackground>      
        </View>
        <View>
        <Image style={{height:50,width:400}} source={require('./img/line.png')} />
        </View>
<ScrollView>
        <View style={{elevation:10, borderColor:'grey', backgroundColor:'white', paddingBottom:50, paddingTop:20, marginTop:20,paddingLeft:10, margin: 22}}>
        <View style={{flexDirection:'row'}}>
        <View>
          <Image style={{height:100,width:100 , marginBottom:20}} source={require('./img/feature1.jpg')} />
        </View>
     
      <View style={{flexDirection:'row'}}>
    <View style={{margin:10}}>
        <Text style={{color:'black',fontWeight:'bold'}}>DJ Cyrius Black</Text>
        <Text style={{marginTop:10,color:'black'}}>Subtitle</Text>
        <View style={{flexDirection:'row',marginTop:10}}>
        <Icon name ="plus" />
        <TextInput style={{width:2,height:2,borderColor:'grey',borderWidth:1}}/>
        <Icon name ="minus" />
        </View>
    </View>
    
    <View style={{margin:10}}>
      <Text style={{color:'#4169e1',fontWeight:'bold'}} >10,000</Text>
      <Icon size={15} style={{marginTop:30,color:'red'}} name="cross" />
    </View>
      </View>
      </View>
         {/* card two here   */}
         <View style={{flexDirection:'column'}}>
<View style={{flexDirection:'row'}}>
        <View>
          <Image style={{height:100,width:100,marginBottom:20}} source={require('./img/feature2.jpg')} />
        </View>
     
      <View style={{flexDirection:'row'}}>
    <View style={{margin:10}}>
        <Text style={{color:'black',fontWeight:'bold'}}>DJ Cyrius Black</Text>
        <Text style={{marginTop:10,color:'black'}}>Subtitle</Text>
        <View style={{flexDirection:'row',marginTop:10}}>
        <Icon name ="plus" />
        <TextInput style={{width:2,height:2,borderColor:'grey',borderWidth:1}}/>
        <Icon name ="minus" />
        </View >
    </View>
    <View>
      <Text style={{color:'#4169e1',fontWeight:'bold'}} >12,000</Text>
      <Icon size={15} style={{marginTop:30,color:'red'}} name="cross" />
    </View>
      </View>
      </View>

        {/* card two here   */}
<View style={{flexDirection:'row'}}>
        <View>
          <Image style={{height:100,width:100,marginBottom:20}} source={require('./img/feature2.png')} />
        </View>
     
      <View style={{flexDirection:'row'}}>
    <View style={{margin:10}}>
        <Text style={{color:'black',fontWeight:'bold'}}>DJ Cyrius Black</Text>
        <Text style={{marginTop:10,color:'black'}}>Subtitle</Text>
        <View style={{flexDirection:'row',marginTop:10}}>
        <Icon name ="plus" />
        <TextInput style={{width:2,height:2,borderColor:'grey',borderWidth:1}}/>
        <Icon name ="minus" />
        </View >
    </View>
    <View>
      <Text style={{color:'#4169e1',fontWeight:'bold'}} >13,000</Text>
      <Icon size={15} style={{marginTop:30,color:'red'}} name="cross" />
    </View>
      </View>
      </View>

      
        {/* card two here   */}
<View style={{flexDirection:'row'}}>
        <View>
          <Image style={{height:100,width:100,marginBottom:20}} source={require('./img/feature3.jpg')} />
        </View>
     
      <View style={{flexDirection:'row'}}>
    <View style={{margin:10}}>
        <Text style={{color:'black',fontWeight:'bold'}}>DJ Cyrius Black</Text>
        <Text style={{marginTop:10,color:'black'}}>Subtitle</Text>
        <View style={{flexDirection:'row',marginTop:10}}>
        <Icon name ="plus" />
        <TextInput style={{width:2,height:2,borderColor:'grey',borderWidth:1}}/>
        <Icon name ="minus" />
        </View >
    </View>
    <View>
      <Text style={{color:'#4169e1',fontWeight:'bold'}} >13,000</Text>
      <Icon size={15} style={{marginTop:30,color:'red'}} name="cross" />
    </View>
      </View>
      </View>
      </View>
      <View>
        <Image style={{height:50,width:300}} source={require('./img/line.png')} />
        </View>
      <View style={{flexDirection:'row'}}>
        <Text style={{fontWeight:'bold',color:'black', fontSize:20}}> Grand Total </Text>
        <Text style={{fontWeight:'bold',color:'black',fontSize:20,marginLeft:80}}>FCFA 48,500</Text>
      </View>
      <TouchableOpacity
            style={styles.button}
            onPress={() => openURL('http://google.com')}
            activeOpacity={1}>
              <Text style={styles.text}>Proceed to CheckOut</Text>
          </TouchableOpacity>
    
        <TouchableOpacity
            style={styles.button1}
            onPress={this._onPress}
            activeOpacity={1}>
           
              <Text style={styles.text}>Empty Cart</Text>
          </TouchableOpacity>
          
   </View>

   </ScrollView>
      </View>
    );
  }
}

export default MyCart;


const styles = StyleSheet.create({
    container: {
      flex: 1
    },
    button: {
      alignItems: 'center',
      justifyContent: 'center',
      backgroundColor: '#96CB7F',
      height:40,
      width:300,
      marginRight:5,
      marginTop:15,
      zIndex: 100,
      marginLeft:5,
      shadowColor: '#00bfff',
    shadowOffset: { width:10, height:10},
    shadowOpacity: 5,
    shadowRadius: 10,
    },
    button1: {
      alignItems: 'center',
      justifyContent: 'center',
      backgroundColor: '#17b23d',
      height:40,
      marginRight:5,
      width:300,
      marginTop:15,
      zIndex: 100,
      marginLeft:5,
      shadowColor: '#00bfff',
    shadowOffset: { width:10, height:10},
    shadowOpacity: 5,
    shadowRadius: 10,
    },
text: {
      color: 'white',
      backgroundColor: 'transparent',
    }
  })