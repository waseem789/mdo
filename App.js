import React, {Component} from 'react';
import {Platform, StyleSheet,SafeAreaView,Image,ImageBackground,Dimensions,ScrollView, Text, View} from 'react-native';
import {createDrawerNavigator , DrawerItems , createStackNavigator} from 'react-navigation';
import HomeScreen from './screen/Home';
import Profile from './screen/Profile';
import MyOrder from './screen/MyOrder';
import Wishlist from './screen/Wishlist';
import Contact from './screen/Contact';
import FAQ from './screen/FAQ';
import Terms from './screen/Terms';
import SignOut from './screen/SignOut';
import SettingsScreen from './screen/Settings'
import MyCart from './screen/MyCart'
import Sport from './screen/Sport'
import Final from './screen/Final'
import Film from './screen/Film'
import {Header,Left,Right} from 'native-base';
import SelectFilm from './screen/SelectFilm'
import Icon from 'react-native-vector-icons/FontAwesome';
const myIcon =  (<Icon name="facebook-f" size={20} color="white" style={{marginLeft:20,marginRight:20}}/>) 
const myIcon1 =  (<Icon name="twitter" size={20} color="white" style={{ marginLeft:20,marginRight:20}}/>) 
const myIcon2 =  (<Icon name="phone" size={20} color="white" style={{ marginLeft:20,marginRight:20}}/>) 
const myIcon3 =  (<Icon name="instagram" size={20} color="white" style={{ marginLeft:20,marginRight:20}}/>) 
const myIcon4 =  (<Icon name="times" size={20} color="white" style={{marginLeft:20,marginRight:20}}/>) 

export default class App extends Component{
  render() {
    return (
      <AppDrawerNavigator />
    );
  }
}

const CustomDrwaerCompnent = (props) => (
<SafeAreaView style={{flex:1}}>
<View style={{height:150,backgroundColor:'white'}}>
<ImageBackground
          resizeMode={'repeat'} 
          style={{flex: 1}}
          source={require('./screen/img/bg.jpg')} >
        <Image source={require('./screen/img/D.jpeg') } style={{height:100,width:100,borderRadius:20,marginTop:15,marginLeft:15, alignItems:'center'}} />
<View style={{flexDirection:'row' ,marginTop:10}}>
{myIcon }
{myIcon1}
{myIcon2}
{myIcon3}
{myIcon4}
</View>
</ImageBackground>

</View>
<ScrollView>
<DrawerItems  {...props}/>
</ScrollView>

</SafeAreaView>


)
const AppDrawerNavigator = createDrawerNavigator({
  Home:HomeScreen,
  Profile:Profile,
  MyCart:MyCart,
  MyOrder:MyOrder,
  Wishlist:Wishlist,
  Contact:Contact,
  FAQ: FAQ,
  Terms: Terms,
  Settings: SettingsScreen,
  SignOut:SignOut,
  Sport:Sport,
  Final:Final,
  Film:Film,

},{
  contentComponent: CustomDrwaerCompnent,
  contentOptions:{
    activeTintColor:'blue'
  },
})

const AppNavigator = createStackNavigator({
  Home:HomeScreen,
  Profile:Profile,
  MyCart:MyCart,
  MyOrder:MyOrder,
  Wishlist:Wishlist,
  Contact:Contact,
  FAQ: FAQ,
  Terms: Terms,
  Settings: SettingsScreen,
  SignOut:SignOut,
  Sport:Sport,
  Final:Final,
  Film:Film,
  SelectFilm:SelectFilm

})


const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  }
})